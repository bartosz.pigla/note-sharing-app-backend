package piglabartosz.notesharing.note.dto;

import java.time.ZonedDateTime;

public record AddNoteRequestDto(
        String title,
        String content,
        String password,
        ZonedDateTime expiryDate
) {
}
