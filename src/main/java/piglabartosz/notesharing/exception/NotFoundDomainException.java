package piglabartosz.notesharing.exception;

public class NotFoundDomainException extends RuntimeException {

    public NotFoundDomainException(String message) {
        super(message);
    }

}
