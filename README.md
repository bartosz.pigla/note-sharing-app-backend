# How to run this project?

1. Set up local database

```
create database "note-sharing-db";
create user "note-sharing-user" with encrypted password '0b22de33e1c9d007ef7d1395d801c766';
grant all privileges on database "note-sharing-db" to "note-sharing-user";
```

2. Set local as active profile in IDE