package piglabartosz.notesharing.note.service;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import piglabartosz.notesharing.note.db.NoteRepository;

@SpringBootTest
public class AddNoteServiceAbstractTest {

    @Autowired
    protected NoteRepository noteRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    protected AddNoteService service;

    @BeforeEach
    void setUp() {
        service = new AddNoteService(noteRepository, passwordEncoder);
        noteRepository.deleteAll();
    }

}
