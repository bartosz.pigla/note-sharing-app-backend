package piglabartosz.notesharing.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import piglabartosz.notesharing.exception.ExpiredDomainException;
import piglabartosz.notesharing.exception.NotFoundDomainException;
import piglabartosz.notesharing.exception.ValidationFailedDomainException;

@ControllerAdvice
class DomainExceptionHandler {

    @ExceptionHandler(value = ValidationFailedDomainException.class)
    ResponseEntity<String> handleBadRequestException(RuntimeException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NotFoundDomainException.class)
    ResponseEntity<String> handleNotFoundException(RuntimeException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ExpiredDomainException.class)
    ResponseEntity<String> handleGoneException(RuntimeException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.GONE);
    }

}
