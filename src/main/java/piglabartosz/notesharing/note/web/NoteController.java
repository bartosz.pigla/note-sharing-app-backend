package piglabartosz.notesharing.note.web;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import piglabartosz.notesharing.note.dto.AddNoteRequestDto;
import piglabartosz.notesharing.note.dto.NoteResponseDto;
import piglabartosz.notesharing.note.dto.PasswordRequestDto;
import piglabartosz.notesharing.note.service.AddNoteService;
import piglabartosz.notesharing.note.service.GetNoteService;

@RestController
@RequestMapping("/note")
@AllArgsConstructor
public class NoteController {

    private final AddNoteService addNoteService;
    private final GetNoteService getNoteService;

    @PostMapping
    public NoteResponseDto addNote(@RequestBody AddNoteRequestDto addNoteRequestDto) {
        return addNoteService.addNote(addNoteRequestDto);
    }

    /**
     * I decided to use POST instead of GET in order to improve security.
     * Password should be in request body, not in url. We do not want to store it in server logs.
     */
    @PostMapping("/{id}")
    public NoteResponseDto getNote(
            @PathVariable String id,
            @RequestBody PasswordRequestDto passwordRequestDto
    ) {
        return getNoteService.getNote(id, passwordRequestDto);
    }

}
