package piglabartosz.notesharing.note.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import piglabartosz.notesharing.exception.ExpiredDomainException;
import piglabartosz.notesharing.exception.NotFoundDomainException;
import piglabartosz.notesharing.exception.ValidationFailedDomainException;
import piglabartosz.notesharing.note.db.NoteEntity;
import piglabartosz.notesharing.note.db.NoteRepository;
import piglabartosz.notesharing.note.dto.PasswordRequestDto;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class GetNoteServiceTest {

    @Autowired
    protected NoteRepository noteRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    protected GetNoteService service;

    @BeforeEach
    void setUp() {
        service = new GetNoteService(noteRepository, passwordEncoder);
        noteRepository.deleteAll();
    }

    @Test
    void successfullyGetsNote() {
        var id = "6d1b57d7-a376-4a9a-badd-312a06edd44f";
        var noteEntity = new NoteEntity(
                UUID.fromString(id),
                "title",
                "content",
                passwordEncoder.encode("password"),
                ZonedDateTime.now().plus(5, ChronoUnit.DAYS)
        );
        noteRepository.save(noteEntity);

        var noteResponseDto = service.getNote(id, new PasswordRequestDto("password"));

        assertThat(noteResponseDto.id()).isEqualTo(id);
        assertThat(noteResponseDto.title()).isEqualTo(noteEntity.getTitle());
        assertThat(noteResponseDto.content()).isEqualTo(noteEntity.getContent());
    }

    @Test
    void failsIfNoteDoesNotExist() {
        noteRepository.deleteAll();

        assertThatThrownBy(() ->
                service.getNote(
                        "6d1b57d7-a376-4a9a-badd-312a06edd44f",
                        new PasswordRequestDto("password")
                )
        )
                .isInstanceOf(NotFoundDomainException.class)
                .hasMessageContaining("Note not found");
    }

    @Test
    void failsIfIdFormatIsIncorrect() {
        var noteEntity = new NoteEntity(
                UUID.randomUUID(),
                "title",
                "content",
                passwordEncoder.encode("password"),
                ZonedDateTime.now().plus(5, ChronoUnit.DAYS)
        );
        noteRepository.save(noteEntity);

        assertThatThrownBy(() -> service.getNote("invalid id", new PasswordRequestDto("password")))
                .isInstanceOf(ValidationFailedDomainException.class)
                .hasMessageContaining("Invalid note id");
    }

    @Test
    void failsIfPasswordIsIncorrect() {
        var id = "6d1b57d7-a376-4a9a-badd-312a06edd44f";
        var noteEntity = new NoteEntity(
                UUID.fromString(id),
                "title",
                "content",
                passwordEncoder.encode("password"),
                ZonedDateTime.now().plus(5, ChronoUnit.DAYS)
        );
        noteRepository.save(noteEntity);

        assertThatThrownBy(() -> service.getNote(id, new PasswordRequestDto("incorrect password")))
                .isInstanceOf(NotFoundDomainException.class)
                .hasMessageContaining("Note not found");
    }

    @Test
    void failsIfDateExpired() {
        var id = "6d1b57d7-a376-4a9a-badd-312a06edd44f";
        var noteEntity = new NoteEntity(
                UUID.fromString(id),
                "title",
                "content",
                passwordEncoder.encode("password"),
                ZonedDateTime.now().minus(1, ChronoUnit.MINUTES)
        );
        noteRepository.save(noteEntity);

        assertThatThrownBy(() -> service.getNote(id, new PasswordRequestDto("password")))
                .isInstanceOf(ExpiredDomainException.class)
                .hasMessageContaining("Note expired");
    }

}
