package piglabartosz.notesharing.note.dto;

public record PasswordRequestDto(String password) {
}
