package piglabartosz.notesharing.note.converter;

import piglabartosz.notesharing.note.db.NoteEntity;
import piglabartosz.notesharing.note.dto.NoteResponseDto;

public class NoteResponseDtoConverter {

    public static NoteResponseDto createFromEntity(NoteEntity entity) {
        return new NoteResponseDto(
                entity.getId().toString(),
                entity.getTitle(),
                entity.getContent()
        );
    }

}
