package piglabartosz.notesharing.note.service;

import org.junit.jupiter.api.Test;
import piglabartosz.notesharing.note.dto.AddNoteRequestDto;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class AddNoteServiceTest extends AddNoteServiceAbstractTest {

    @Test
    void successfullySavesNoteAndInitializesAllFields() {
        var addNoteRequestDto = new AddNoteRequestDto(
                "title",
                "test content",
                "test password",
                ZonedDateTime.now().plus(5, ChronoUnit.DAYS)
        );

        service.addNote(addNoteRequestDto);

        var savedNotes = noteRepository.findAll();
        assertThat(savedNotes).hasSize(1);

        var note = noteRepository.findAll().get(0);
        assertThat(note.getTitle()).isEqualTo(addNoteRequestDto.title());
        assertThat(note.getContent()).isEqualTo(addNoteRequestDto.content());
        assertThat(passwordEncoder.matches(addNoteRequestDto.password(), note.getPassword())).isTrue();
        assertThat(note.getExpiryDate().toInstant().toEpochMilli())
                .isEqualTo(addNoteRequestDto.expiryDate().toInstant().toEpochMilli());
    }

}
