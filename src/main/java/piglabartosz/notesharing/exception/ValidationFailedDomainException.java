package piglabartosz.notesharing.exception;

public class ValidationFailedDomainException extends RuntimeException {
    public ValidationFailedDomainException(String message) {
        super(message);
    }
}
