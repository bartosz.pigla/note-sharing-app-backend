package piglabartosz.notesharing.note.service;

import org.junit.jupiter.api.Test;
import piglabartosz.notesharing.exception.ValidationFailedDomainException;
import piglabartosz.notesharing.note.dto.AddNoteRequestDto;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AddNoteServiceTitleValidationTest extends AddNoteServiceAbstractTest {

    @Test
    void failsIfTitleIsNull() {
        var addNoteRequestDto = createDtoWithTitle(null);

        assertThatThrownBy(() -> service.addNote(addNoteRequestDto))
                .isInstanceOf(ValidationFailedDomainException.class)
                .hasMessageContaining("Title cannot be empty");
    }

    @Test
    void failsIfTitleIsEmpty() {
        var addNoteRequestDto = createDtoWithTitle("");

        assertThatThrownBy(() -> service.addNote(addNoteRequestDto))
                .isInstanceOf(ValidationFailedDomainException.class)
                .hasMessageContaining("Title cannot be empty");
    }

    @Test
    void failsIfTitleIsTooLong() {
        var addNoteRequestDto = createDtoWithTitle("X".repeat(101));

        assertThatThrownBy(() -> service.addNote(addNoteRequestDto))
                .isInstanceOf(ValidationFailedDomainException.class)
                .hasMessageContaining("Too long title");
    }

    private AddNoteRequestDto createDtoWithTitle(String title) {
        return new AddNoteRequestDto(
                title,
                "test content",
                "test password",
                ZonedDateTime.now().plus(5, ChronoUnit.DAYS)
        );
    }

}
