package piglabartosz.notesharing.note.dto;

public record NoteResponseDto(
        String id,
        String title,
        String content
) {
}
