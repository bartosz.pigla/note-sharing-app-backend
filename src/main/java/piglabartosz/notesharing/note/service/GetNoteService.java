package piglabartosz.notesharing.note.service;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import piglabartosz.notesharing.exception.ExpiredDomainException;
import piglabartosz.notesharing.exception.NotFoundDomainException;
import piglabartosz.notesharing.exception.ValidationFailedDomainException;
import piglabartosz.notesharing.note.converter.NoteResponseDtoConverter;
import piglabartosz.notesharing.note.db.NoteRepository;
import piglabartosz.notesharing.note.dto.NoteResponseDto;
import piglabartosz.notesharing.note.dto.PasswordRequestDto;

import java.time.ZonedDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class GetNoteService {

    private final NoteRepository noteRepository;
    private final PasswordEncoder passwordEncoder;

    public NoteResponseDto getNote(String id, PasswordRequestDto passwordRequestDto) {
        var uuid = getUUID(id);
        var note = noteRepository.findById(uuid).orElseThrow(() -> new NotFoundDomainException("Note not found"));
        validatePassword(passwordRequestDto.password(), note.getPassword());
        validateExpiryDate(note.getExpiryDate());

        return NoteResponseDtoConverter.createFromEntity(note);
    }

    private UUID getUUID(String id) {
        try {
            return UUID.fromString(id);
        } catch (Exception exception) {
            throw new ValidationFailedDomainException("Invalid note id");
        }
    }

    private void validatePassword(String passwordFromDto, String encryptedNotePassword) {
        if (!passwordEncoder.matches(passwordFromDto, encryptedNotePassword)) {
            // message is the same as for non-existent note in order to improve security
            throw new NotFoundDomainException("Note not found");
        }
    }

    private void validateExpiryDate(ZonedDateTime expiryDate) {
        if (expiryDate.isBefore(ZonedDateTime.now())) {
            throw new ExpiredDomainException("Note expired");
        }
    }

}
