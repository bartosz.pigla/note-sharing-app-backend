package piglabartosz.notesharing.exception;

public class ExpiredDomainException extends RuntimeException {

    public ExpiredDomainException(String message) {
        super(message);
    }

}
