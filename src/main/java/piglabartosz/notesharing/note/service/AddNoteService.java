package piglabartosz.notesharing.note.service;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Range;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import piglabartosz.notesharing.exception.ValidationFailedDomainException;
import piglabartosz.notesharing.note.converter.NoteResponseDtoConverter;
import piglabartosz.notesharing.note.db.NoteEntity;
import piglabartosz.notesharing.note.db.NoteRepository;
import piglabartosz.notesharing.note.dto.AddNoteRequestDto;
import piglabartosz.notesharing.note.dto.NoteResponseDto;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.regex.Pattern;

import static io.micrometer.common.util.StringUtils.isEmpty;

@Service
@AllArgsConstructor
public class AddNoteService {

    private static final int TITLE_MAX_LENGTH = 100;
    private static final int CONTENT_MAX_LENGTH = 3_000;
    private static final Range<Integer> PASSWORD_LENGTH_RANGE = Range.between(10, 50);
    private static final Pattern PASSWORD_SPECIAL_CHARS_PATTERN =
            Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
    private static final long EXPIRY_DATE_MAX_AGE_IN_DAYS = 10L;
    private final NoteRepository noteRepository;
    private final PasswordEncoder passwordEncoder;

    public NoteResponseDto addNote(AddNoteRequestDto addNoteRequestDto) {
        validateTitle(addNoteRequestDto.title());
        validateContent(addNoteRequestDto.content());
        validatePassword(addNoteRequestDto.password());
        validateExpiryDate(addNoteRequestDto.expiryDate());

        var note = new NoteEntity(
                UUID.randomUUID(),
                addNoteRequestDto.title(),
                addNoteRequestDto.content(),
                passwordEncoder.encode(addNoteRequestDto.password()),
                addNoteRequestDto.expiryDate()
        );

        var savedNote = noteRepository.save(note);

        return NoteResponseDtoConverter.createFromEntity(savedNote);
    }

    private void validateTitle(String title) {
        if (isEmpty(title)) {
            throw new ValidationFailedDomainException("Title cannot be empty");
        }

        if (title.length() > TITLE_MAX_LENGTH) {
            throw new ValidationFailedDomainException("Too long title");
        }
    }

    private void validateContent(String content) {
        if (isEmpty(content)) {
            throw new ValidationFailedDomainException("Content cannot be empty");
        }

        if (content.length() > CONTENT_MAX_LENGTH) {
            throw new ValidationFailedDomainException("Too long content");
        }
    }

    private void validatePassword(String password) {
        if (isEmpty(password)) {
            throw new ValidationFailedDomainException("Password cannot be empty");
        }

        if (!PASSWORD_LENGTH_RANGE.contains(password.length())) {
            throw new ValidationFailedDomainException("Password length out of range");
        }

        var matcher = PASSWORD_SPECIAL_CHARS_PATTERN.matcher(password);
        if (!matcher.find()) {
            throw new ValidationFailedDomainException("Password must contain special characters");
        }
    }

    private void validateExpiryDate(ZonedDateTime expiryDate) {
        if (expiryDate == null) {
            throw new ValidationFailedDomainException("Expiry date cannot be empty");
        }

        var currentTime = ZonedDateTime.now();

        if (!expiryDate.isAfter(currentTime)) {
            throw new ValidationFailedDomainException("Expiry date cannot be before current time");
        }

        if (ChronoUnit.DAYS.between(currentTime, expiryDate) > EXPIRY_DATE_MAX_AGE_IN_DAYS) {
            throw new ValidationFailedDomainException("Invalid expiry date");
        }
    }

}
